import java.util.Scanner;
public class Zinseszins {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		
		System.out.println("Wie viele Jahre soll der Sparvertrag gehen?");
		int spar = scan.nextInt();
		
		System.out.println("Wie viel Kapital (In Euro) m�chten Sie anlegen?");
		double kapi = scan.nextDouble();
		
		System.out.println("Wie hoch ist der Zinssatz?");
		double zins = scan.nextDouble();
		
		int timer = 1;
		double kapizins;
		
		while (spar > 0) {
			
			kapi = kapi * zins;
			
			System.out.println("Das Kapital nach "+ timer++ + " Jahren: " + kapi);
			
			spar--;
		}
		
	}

}
