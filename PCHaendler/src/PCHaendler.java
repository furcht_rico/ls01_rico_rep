 import java.util.Scanner;

public class PCHaendler {

	public static String liesString(String text) {
	Scanner myScanner = new Scanner(System.in);
	System.out.println(text);
	String art = myScanner.next();
	return art;
	}
	
	public static int liesInt(String text ) {
	Scanner myScanner = new Scanner(System.in);
	System.out.println(text);
	int anz = myScanner.nextInt();
	return anz;	
	}
	
	public static double nettopreis(String text) {
	Scanner myScanner = new Scanner(System.in);
	System.out.println(text);
	double preis = myScanner.nextDouble();
	return preis;	
	}
	
	public static double Mehrwertsteuer (String text) {
	Scanner myScanner = new Scanner(System.in);
	System.out.println(text);
	double mwst = myScanner.nextDouble();
	return mwst;	
	}	
		
	
	
	public static void main(String[] args) {
	Scanner myScanner = new Scanner(System.in);
	
	String artikel = liesString("Was m�chten Sie bestellen?");
		
	int anzahl = liesInt("Geben Sie eine Anzahl ein: ");
		
	double preis = nettopreis("Geben Sie den Nettopreis ein: ");
		
	double mwst = Mehrwertsteuer("Geben Sie den Mehrwertsteuersatz in Prozent ein:");

		// Verarbeiten
	
	double nettogesamtpreis = anzahl * preis;
	double bruttogesamtpreis = nettogesamtpreis * (1 + mwst / 100);

		// Ausgeben

	System.out.println("\t\t Rechnung:");
	System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
	System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
	
	
	}
}

