import java.util.Scanner;
public class Temperaturrechnung {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Bitte den Startwert in Celcius eingeben: ");
		double s = scan.nextDouble();
		
		System.out.println("Bitte den Endwert in Celcius eingeben: ");
		double e = scan.nextDouble();
		
		System.out.println("Bitte die Schrittweite in Grad Celcius eingeben: ");
		double w = scan.nextDouble();
		
		
		while (s <= e) {
			
			double f = (s * 9/5) + 32; 
					
			System.out.println(s+"�C        "+f+"�F");
			
			s = s + w;
			
	}
		
	}

}
