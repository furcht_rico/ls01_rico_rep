import java.util.Scanner;

public class SchleifenBeispiele 
{
    public static void main(String[] args) 
    {
        //Array erstellen    	
        int [] zahlen = new int [4];
        
        //Array f�llen
        for (int i = 0; i < 4; i++)
        {
            zahlen [i] = i + 1;
        }
        
        //Array ausgeben
        System.out.print("[ ");
        for (int i = 0; i < 4; i++)
        {
            System.out.print(zahlen[i] + " ");
        }
        System.out.println("]");
    }

}