import java.util.Scanner;

public class BMI {

	public static void main(String[] args) {
		Scanner myscan = new Scanner(System.in);
		
		System.out.println("Geben Sie Ihr Körpergewicht in Kg an!");
		double koepergewicht = myscan.nextDouble();
		
		System.out.println("Geben Sie Ihre Körpergröße in m an!");
		double koepergroeße = myscan.nextDouble();
		
		System.out.println("Sind Sie Männlich dann m oder w für Weiblich!");
		char geschlecht = myscan.next().charAt(0);
		
		BMI(koepergewicht,koepergroeße,geschlecht);
		
	}

	public static void BMI(double koerpergewicht, double koerpergroeße, char geschlecht) {
		double bmirechnung = koerpergewicht / (koerpergroeße * koerpergroeße);
		
		//Untergewichtig
		if (bmirechnung <= 20 && geschlecht == 'm') {
			System.out.println("Sie sind Untergewichtig da Ihr BMI " + bmirechnung + " beträgt!");
		}
		if (bmirechnung <= 19 && geschlecht == 'w') {
			System.out.println("Sie sind Untergewichtig da Ihr BMI " + bmirechnung + " beträgt!");
		}
		
		//Normalgewichtig
		if (bmirechnung > 20 && bmirechnung <= 25 && geschlecht == 'm') {
			System.out.println("Sie sind Normalgewichtig da Ihr BMI " + bmirechnung + " beträgt!");
		}
		if (bmirechnung < 19 && bmirechnung <= 24 && geschlecht == 'w') {
			System.out.println("Sie sind Normalgewichtig da Ihr BMI " + bmirechnung + " beträgt!");
		}
		
		//Übergewichtig
		if (bmirechnung > 25 && geschlecht == 'm') {
			System.out.println("Sie sind Übergewichtig da Ihr BMI " + bmirechnung + " beträgt!");
		}
		if (bmirechnung > 24 && geschlecht == 'w') {
			System.out.println("Sie sind Übergewichtig da Ihr BMI " + bmirechnung + " beträgt!");
		}
		
	}
	
}
