/**
  *   Aufgabe:  Recherechieren Sie im Internet !
  * 
  *   Sie dürfen nicht die Namen der Variablen verändern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.0 from 21.08.2019
  * @author << Ihr Name >>
  */

public class WeltDerZahlen {

  public static void main(String[] args) {
    
    /*  *********************************************************
    
         Zuerst werden die Variablen mit den Werten festgelegt!
    
    *********************************************************** */
    // Im Internet gefunden ?
    // Die Anzahl der Planeten in unserem Sonnesystem                    
    int anzahlPlaneten = 9 ; 
    
    // Anzahl der Sterne in unserer Milchstraße
    int    anzahlSterne = 200;
    
    // Wie viele Einwohner hat Berlin?
    double    bewohnerBerlin = 3.8;
    
    // Wie alt bist du?  Wie viele Tage sind das?
    
    int  alterTage = 7066;
    
    // Wie viel wiegt das schwerste Tier der Welt?
    // Schreiben Sie das Gewicht in Kilogramm auf!
    int   gewichtKilogramm = 190000;
    
    // Schreiben Sie auf, wie viele km² das größte Land er Erde hat?
    double   flaecheGroessteLand = 17.1;
    
    // Wie groß ist das kleinste Land der Erde?
    
    double   flaecheKleinsteLand = 0.44; 
    
    
    
    
    /*  *********************************************************
    
         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
    
    *********************************************************** */
    
    System.out.println("Anzhahl der Planeten: " + anzahlPlaneten);
    
    System.out.println("Anzahl der Sterne: "+ anzahlSterne);
    System.out.println("Anzahl der Bewohner in Berlin: "+ bewohnerBerlin);
    System.out.println("Anzahl der Tage: "+ alterTage + " Tage");
    System.out.println("Gewicht des schwersten Tiers: "+ gewichtKilogramm + " Kg");
    System.out.println("Fl�che des gr��ten Landes: "+ flaecheGroessteLand + " Km�");
    System.out.println("Fl�che des kleinten Landes: "+ flaecheKleinsteLand + " Km�");
    System.out.println(" *******  Ende des Programms  ******* ");
    
  }
}

